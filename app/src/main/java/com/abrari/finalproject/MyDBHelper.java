package com.abrari.finalproject;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.abrari.finalproject.models.Patient;

import lombok.val;

//////////////////////////Part of the source code is taken off. Please watch the video lesson for the missing code///////////////
public class MyDBHelper extends SQLiteOpenHelper {

    private static MyDBHelper instance;

    private SQLiteDatabase sqLiteDatabase;

    private static final String DATABASE_NAME="Final.db";
    private static final String TABLE_NAME="final_details";

    private static final String GUID ="_guid";
    private static final String RELATEDDOCTORGUID ="RelatedDoctorGUID";
    private static final String NAME="Name";
    private static final String ADDRESS="Address";
    private static final String LASTGLUCOSELEVEL="LastGlucoseLevel";
    private static final String EMAIL="Email";

    private static final String CREATE_TABLE="CREATE TABLE "+TABLE_NAME+
            "(" +GUID +" INTEGER PRIMARY KEY AUTOINCREMENT,"+
            RELATEDDOCTORGUID+ " VARCHAR(255),"+
            NAME+ " VARCHAR(255),"+
            ADDRESS +" VARCHAR(255)," +
            LASTGLUCOSELEVEL +" VARCHAR(15)," +
            EMAIL +" VARCHAR(255));" ;
    private static final String CREATE_TABLE_UPD="CREATE TABLE "+TABLE_NAME+
            "(" +GUID +" INTEGER PRIMARY KEY AUTOINCREMENT,"+
            RELATEDDOCTORGUID+ " VARCHAR(255),"+
            NAME+ " VARCHAR(255),"+
            ADDRESS +" VARCHAR(255)," +
            LASTGLUCOSELEVEL +" VARCHAR(15)," +
            EMAIL +" VARCHAR(255));" ;

    private static final String DROP_TABLE="DROP TABLE IF EXISTS "+TABLE_NAME;
    private static final String SELECT_ALL="SELECT * FROM " + TABLE_NAME ;

    private static final String SELECT_WHERE_GUID = String.format("SELECT * FROM %s WHERE %1$s.%s=", TABLE_NAME, GUID);

    private static final int VERSION_NUMBER=1;
    private Context context;

    private MyDBHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, VERSION_NUMBER);
        this.context=context;
    }

    @Override
    //Only once at the time of creation DB
    //Create the DB here
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        try{
            sqLiteDatabase.execSQL(DROP_TABLE);
            showToast("Called dropTable()");
            sqLiteDatabase.execSQL(CREATE_TABLE);
            showToast("Called onCreate() to create table");

        }catch (Exception e){
            showToast("Exception: " + e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        try{
            sqLiteDatabase.execSQL(DROP_TABLE);
            showToast("Called dropTable()");

        }catch (Exception e){
            showToast("Exception: " + e);
        }

        try{
            sqLiteDatabase.execSQL(CREATE_TABLE_UPD);
            showToast("Called CREATE_TABLE_UPD");

        }catch (Exception e){
            showToast("Exception: " + e);
        }
    }

    public long insertTable(String relatedDoctorGUID, String name, String address, String lastGlucoseLevel, String email){
        ContentValues contentValues=new ContentValues();
        contentValues.put("RelatedDoctorGUID", relatedDoctorGUID);
        contentValues.put("Name", name);
        contentValues.put("Address", address);
        contentValues.put("LastGlucoseLevel", lastGlucoseLevel);
        contentValues.put("Email", email);

        showToast(contentValues.toString());
        sqLiteDatabase=this.getWritableDatabase();
        long rowID=sqLiteDatabase.insert(TABLE_NAME,null,contentValues);

        return rowID;
    }

    public Cursor displayAllData(){
        sqLiteDatabase=this.getWritableDatabase();
        return sqLiteDatabase.rawQuery(SELECT_ALL, null);
    }

    public Cursor findByGUID(String guid) {

        sqLiteDatabase = getWritableDatabase();
        val cursor = sqLiteDatabase.rawQuery(SELECT_WHERE_GUID + guid, null);
        if(cursor != null) {
            cursor.moveToFirst();;
        }
        return cursor;
    }

    public boolean savePatient(Patient patient) {


        sqLiteDatabase=this.getWritableDatabase();

        try {

            val values = new ContentValues();
            values.put(GUID, patient.getGuid());
            values.put(RELATEDDOCTORGUID, patient.getRelatedDoctorGUID());
            values.put(NAME, String.format("%s %s", patient.getFirstName(), patient.getLastName()));
            values.put(ADDRESS, patient.getAddress());
            values.put(LASTGLUCOSELEVEL, patient.getLastGlucoseLevel());
            values.put(EMAIL, patient.getEmail());

            long id = sqLiteDatabase.update(TABLE_NAME, values, "_guid = ?", new String[]{ patient.getGuid() });
            System.out.println(id);
            return true;
        }
        catch (Exception e) {

            e.printStackTrace();
            return false;
        }
        finally {
            sqLiteDatabase.close();
        }
    }

    public boolean updateGlucose(String lastGlucoseLevel){
        ContentValues contentValues=new ContentValues();
        contentValues.put("LastGlucoseLevel", lastGlucoseLevel);
        showToast(contentValues.toString());
        sqLiteDatabase=this.getWritableDatabase();
        try{
            sqLiteDatabase.execSQL("UPDATE '"+TABLE_NAME+"' SET '"+LASTGLUCOSELEVEL+"'='"+contentValues+"' WHERE _guid=1");
            return true;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    public boolean updateData(String Guid, String relatedDoctorGUID, String name, String address, String lastGlucoseLevel, String email){
        ContentValues contentValues=new ContentValues();
        contentValues.put("_guid", Guid);
        contentValues.put("RelatedDoctorGUID", relatedDoctorGUID);
        contentValues.put("Name", name);
        contentValues.put("Address", address);
        contentValues.put("LastGlucoseLevel", lastGlucoseLevel);
        contentValues.put("Email", email);
        showToast(contentValues.toString());
        sqLiteDatabase=this.getWritableDatabase();
        try{
            int rowNumUpd= sqLiteDatabase.update(TABLE_NAME,
                    contentValues,"_guid = ?", new String[]{Guid});
            if (rowNumUpd>=0){
                return true;
            }
            else{
                return false;
            }
        }catch (Exception e){
            showToast("Exception: " + e);
            return false;
        }
    }

    public Integer deleteData(String Guid){
        sqLiteDatabase=this.getWritableDatabase();
        try{
            SQLiteDatabase db = this.getWritableDatabase();
            return db.delete(TABLE_NAME, "_guid = ?",new String[] {Guid});
        }catch (Exception e) {
            showToast("Exception: "+e);
            return 0;
        }
    }

    private void showToast(String message){
        Toast toast;
        toast=Toast.makeText(this.context, message, Toast.LENGTH_LONG);
        toast.show();
    }

    public static synchronized MyDBHelper getInstance(Context context) {

        if(instance == null) {

            instance = new MyDBHelper(context.getApplicationContext());
        }

        return instance;
    }

}

package com.abrari.finalproject;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btnEdit, btnRemove, btnNotify, btnReset, btnShowAll;
    private GestureDetector gestureDetector;
    private boolean consumed;

    MyDBHelper myDBHelper;
    SQLiteDatabase sqLiteDatabase;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        consumed = false;

        myDBHelper = MyDBHelper.getInstance(this);
        sqLiteDatabase = myDBHelper.getWritableDatabase();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        btnEdit = findViewById(R.id.btnEditId);
        btnRemove = findViewById(R.id.btnRemoveId);
        btnNotify = findViewById(R.id.btnNotifyId);
        btnReset = findViewById(R.id.btnResetId);
        btnShowAll = findViewById(R.id.btnShowId);


        btnReset.setOnClickListener(MainActivity.this);
        btnNotify.setOnClickListener(MainActivity.this);
        btnRemove.setOnClickListener(MainActivity.this);
        btnEdit.setOnClickListener(MainActivity.this);
        btnShowAll.setOnClickListener(MainActivity.this);

        gestureDetector = new GestureDetector(getApplicationContext(), new GestureDetector.OnGestureListener() {
            @Override
            public boolean onDown(MotionEvent e) {
                return false;
            }

            @Override
            public void onShowPress(MotionEvent e) {

            }

            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                return false;
            }

            @Override
            public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
                return false;
            }

            @SuppressLint("DefaultLocale")
            @Override
            public void onLongPress(MotionEvent e) {

                showToast(String.format("Number of patients: %d", myDBHelper.displayAllData().getCount()));
                consumed = true;
            }

            @Override
            public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
                return false;
            }
        });

        btnNotify.setOnTouchListener((v, event) -> gestureDetector.onTouchEvent(event));
    }

    public void onClick(View v) {

        if(consumed) {
            consumed = false;
            return;
        }

        if (v.getId()==R.id.btnEditId) {

            final Intent intent = new Intent(this, EditPatientActivity.class);
            startActivity(intent);
        }
        else if (v.getId()==R.id.btnRemoveId) {

            final Intent intent = new Intent(this, DeletePatientActivity.class);
            startActivity(intent);
        }
        else if (v.getId()==R.id.btnNotifyId) {

            showToast("Send Glucose Level button clicked");
            boolean sendData= myDBHelper.updateGlucose("New Glucose Level Registered");
            if (sendData){
                showToast("Glucose Level Sent");
            }
            else{
                showToast("Glucose Level Sending Error");
            }
            displayAllData();

        }
        else if(v.getId() == R.id.btnShowId) {

            displayAllData();
        }
        else if (v.getId()==R.id.btnResetId) {

            showToast("Reset button clicked");
            myDBHelper.onCreate(sqLiteDatabase);
            long rowID = MyDBHelper.getInstance(this).insertTable("Related Doctor GUID 1", "Patient Name", "Patient Address", "Last Glucose Level", "Patient Email");
            if (rowID==-1){
                showToast("Row insertion failed");
            }
            else{
                showToast("Row: " + rowID  + " insertion worked");
                displayAllData();
            }
        }
    }


    public void showToast(String s1){
        Toast tst;
        tst = Toast.makeText(this,s1,Toast.LENGTH_LONG);
        tst.show();
    }

    private void displayAllData(){
        Cursor resultSet=myDBHelper.displayAllData();
        if (resultSet.getCount() <= 0){
            showToast("Display error: No row selected");
            showData("Error", "No data found");
            return;
        }

        StringBuilder sb= new StringBuilder();

        while(resultSet.moveToNext()){
            sb.append("\nGUID: ").append(resultSet.getString(0));
            sb.append("\nRelatedDoctorGUID: ").append(resultSet.getString(1));
            sb.append("\nName: ").append(resultSet.getString(2));
            sb.append("\nAddress: ").append(resultSet.getString(3));
            sb.append("\nLastGlucoseLevel: ").append(resultSet.getString(4));
            sb.append("\nEmail: ").append(resultSet.getString(5)).append("\n\n");
        }
        showToast(sb.toString());
        showData("Results", sb.toString());

    }

    private void showData(String title, String data){
        //AlertDialog Builder
        AlertDialog.Builder alBuilder= new AlertDialog.Builder(this);
        //alBuilder.set
        alBuilder.setTitle(title);
        alBuilder.setMessage(data);
        alBuilder.setCancelable(true);
        alBuilder.show();
    }
}

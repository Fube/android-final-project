package com.abrari.finalproject;

import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.abrari.finalproject.models.Patient;

import java.util.Arrays;

import lombok.val;

public class EditPatientActivity extends AppCompatActivity implements View.OnClickListener {

    private MyDBHelper myDBHelper;

    private EditText
        guidEdtTxt,
        fNameEdtTxt,
        lNameEdtTxt,
        emailEdtTxt,
        addressEdtTxt;

    private Button saveBtn;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_patient_layout);

        myDBHelper = MyDBHelper.getInstance(this);

        guidEdtTxt = findViewById(R.id.edit_guid);
        fNameEdtTxt = findViewById(R.id.edit_name);
        lNameEdtTxt = findViewById(R.id.edit_last_name);
        emailEdtTxt = findViewById(R.id.edit_email);
        addressEdtTxt = findViewById(R.id.edit_address);
        saveBtn = findViewById(R.id.edit_save);

        saveBtn.setOnClickListener(this);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onClick(View v) {

        val patient = getByGUID(guidEdtTxt.getText().toString());
        if(patient == null) {
            showToast("No patient found with guid: " + guidEdtTxt.getText());
            return;
        }

        patient.setFirstName(getEditTextValue(fNameEdtTxt));
        patient.setLastName(getEditTextValue(lNameEdtTxt));
        patient.setEmail(getEditTextValue(emailEdtTxt));
        patient.setAddress(getEditTextValue(addressEdtTxt));

        if(myDBHelper.savePatient(patient)) {
            showToast("Saved");
        }
        else {
            showToast("Unable to save");
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private Patient getByGUID(String guid) {

        val cursor = myDBHelper.findByGUID(guid);

        if(cursor.getCount() <= 0) {
            return null;
        }

        val name = cursor.getString(2);
        val bits = name.split(" ");

        val fName = bits[0];
        val lName = String.join(" ", Arrays.copyOfRange(bits, 1, bits.length));

        return Patient.builder()
                .guid(cursor.getString(0))
                .relatedDoctorGUID(cursor.getString(1))
                .firstName(fName)
                .lastName(lName)
                .address(cursor.getString(3))
                .lastGlucoseLevel(cursor.getString(4))
                .email(cursor.getString(5))
                .build();
    }

    public void showToast(String s1){
        Toast tst;
        tst = Toast.makeText(this,s1,Toast.LENGTH_LONG);
        tst.show();
    }

    private String getEditTextValue(EditText edt) {
        return edt.getText().toString();
    }
}

package com.abrari.finalproject.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Patient {

    private String guid;
    private String relatedDoctorGUID;
    private String firstName;
    private String lastName;
    private String address;
    private String lastGlucoseLevel;
    private String email;
}

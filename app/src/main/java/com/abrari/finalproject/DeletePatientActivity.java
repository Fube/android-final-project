package com.abrari.finalproject;


import android.app.AlertDialog;
import android.content.DialogInterface;

import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AlertDialog.Builder;

import com.abrari.finalproject.models.Patient;

import java.util.Arrays;

import lombok.val;

public class DeletePatientActivity extends AppCompatActivity implements View.OnClickListener {

    private MyDBHelper myDBHelper;
    private EditText guidEdtTxt;
    private Button deleteBtn;

    private AlertDialog mDialog;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.delete_patient_layout);

        myDBHelper = MyDBHelper.getInstance(this);

        guidEdtTxt = findViewById(R.id.edit_guid);
        deleteBtn = findViewById(R.id.deleteBtn);

        deleteBtn.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        val patient = getByGUID(guidEdtTxt.getText().toString());
        if(patient == null) {
            showToast("No patient found with guid: " + guidEdtTxt.getText());
            return;
        }else{
            alertUser();
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private Patient getByGUID(String guid) {

        val cursor = myDBHelper.findByGUID(guid);

        if(cursor.getCount() <= 0) {
            return null;
        }

        val name = cursor.getString(2);
        val bits = name.split(" ");

        val fName = bits[0];
        val lName = String.join(" ", Arrays.copyOfRange(bits, 1, bits.length));

        return Patient.builder()
                .guid(cursor.getString(0))
                .relatedDoctorGUID(cursor.getString(1))
                .firstName(fName)
                .lastName(lName)
                .address(cursor.getString(3))
                .lastGlucoseLevel(cursor.getString(4))
                .email(cursor.getString(5))
                .build();
    }


    public void showToast(String s1){
        Toast tst;
        tst = Toast.makeText(this,s1,Toast.LENGTH_LONG);
        tst.show();
    }

    public void alertUser() {

        AlertDialog.Builder alBuilder = new AlertDialog.Builder(this);

        alBuilder.setTitle("Alert");
        alBuilder.setMessage("Are you sure to delete user ID: " + guidEdtTxt.getText());
        alBuilder.setCancelable(false);

        alBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                Integer deleteRows = myDBHelper.deleteData(guidEdtTxt.getText().toString());
                if(deleteRows >= 0) {
                    showToast("Patient successfully deleted");
                }
                else{
                    showToast("Unable to delete");
                }

            }
        });

        alBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        AlertDialog alertDialog = alBuilder.create();

        alertDialog.show();
    }

}
